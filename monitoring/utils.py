# Imports
import json
import requests

# Global variables
logfile = "/home/ubuntu/.monitoring/error.log"
webhook_bad  = "https://discord.com/api/webhooks/935526852246798396/N5MpvmnALbq10DoGIeH8xLfMpj-j9m5n5kQoU1E0sdRIvo3UJn_ffDaVYAGz_4yHCcWd"
webhook_good = "https://discord.com/api/webhooks/935541051635138600/tfdSJBXzuVq_nxy5tfoZfmtmi9T5qMtegASJRPsAkY6VnHbggGliRCl12KtpDpF4FfvP"

# Functions
def getErrorDiff(errors, log):
    """
    Checks the reported errors against the previously reported errors.
    It will two dictionaries, one that contains new errors, and one that contains fixed errors.
    """
    good = {}
    bad = {}
    for key in errors.keys():
        b = [err for err in errors[key] if err not in log[key]]
        if key in log.keys():
            g = [err for err in log[key] if err not in errors[key]]
        else:
            g = []
        if len(b) > 0:
            bad[key] = b
        if len(g) > 0:
            good[key] = g
    return good, bad


def writeErrorsToDisk(errors):
    """
    Writes the errors to a log file that is used to compare results between tests.
    """
    with open(logfile, "w") as f:
        f.write(json.dumps(errors))
    return None

def readErrorsFromDisk():
    """
    Returns a dictionary containing the previously logged errors
    """
    with open(logfile, "r") as f:
        data = json.load(f)
        return data
        

def reportGoodNews(good):
    """
    Writes the good news to the discord webhook
    """
    msg =  "Stonks Bot can report the following good news:\n"

    for key in good.keys():
        msg += "```\n"
        msg += key + ":\n"
        for i in good[key]:
            msg += f"\t{i}\n"
        msg += "```\n"

    data = {"content": msg}
    requests.post(webhook_good, json=data)


def reportBadNews(bad):
    """
    Writes the bad news to the discord webhook
    """
    msg =  "Not Stonks Bot can report the following bad news:\n"

    for key in bad.keys():
        msg += "```\n"
        msg += key + ":\n"
        for i in bad[key]:
            msg += f"\t{i}\n"
        msg += "```\n"

    data = {"content": msg}
    requests.post(webhook_bad, json=data)

def startHost(host):
    host.start()
