#!python3

"""
This is a custom library that contains functions that will get information from the network
"""

# Imports
from keystoneauth1 import loading
from keystoneauth1 import session
from keystoneauth1.identity import v3
import novaclient.v2.client as nvclient
import requests

# Functions
def authenticateOpenStack():
    """
    Authenticates to OpenStack Compute with our application credentials, and returns the nova session
    """
    applicationCredential = v3.ApplicationCredentialMethod(
            application_credential_secret = "LJdylxGB9JXCI91wbeRMMBnGbLFmMmF4v6NTZLMGlRr_J9E_tlR434q1hVryZZB4mdDnYRrWuaqbgj2TLMt9Gg",
            application_credential_id = "388dcdeadf5643a1927c17fb23bedf3a"
            )
    
    auth = v3.Auth(
            auth_url = "https://api.skyhigh.iik.ntnu.no:5000",
            auth_methods = [applicationCredential]
            )

    sess = session.Session(auth=auth)
    nova = nvclient.client.Client('2', session=sess)

    return nova

def getHosts(nvclient):
    """
    Returns two dictionaries of the hosts IP addresses in our OpenStack
    """
    local_addr = {}
    floating_addr = {}
    for server in nvclient.servers.list():
        local_addr[server.name] = server.addresses["imt3003"][0]["addr"]
        if len(server.addresses["imt3003"]) == 2:
            floating_addr[server.name] = server.addresses["imt3003"][1]["addr"]
    return local_addr, floating_addr

def getHostStatus(host):
    """
    Returns the status of the named host
    """ 
    if host.status == "ACTIVE":
        return True
    return False
