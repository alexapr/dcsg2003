#!python3

"""
This script is made in order to monitor our project in DCSG2003.
It was made by Alexander P. Roaas and Steinar M. Myhre under the company name "jndi-boi AS".
"""
# Imports
from novaclient.client import Client

# Import project files
import netinfo  # Custom library to gain information about the network
import tests	# Custom library containing our tests
import utils
import inspect

# Global variables
nvclient = netinfo.authenticateOpenStack()
servers = nvclient.servers.list()

# Create dictionaries for the servers and their IPs for simple tests
local_addr, ext_addr = netinfo.getHosts(nvclient)

# Dictionaries for errors
errors = {}
for key in local_addr.keys():
	errors[key] = []

# Run tests

####
## Active test: Checks that VM's are turned on, and if not attempts to turn them back on
####
for server in servers:
    test = tests.activetest(server)
    if test:
        errors[server.name].append(test)

####
## Ping test: Verify that the servers are up and responding
####
for key in local_addr.keys():
	test = tests.pingtest(local_addr[key])
	if test:
		errors[key].append(test)
####
## Web test: Fetch the website and verify no errors
####
test = tests.webtest(ext_addr["node1"])
if test:
	errors["node1"].append(test)


# Fetch the logged results from previous test
try:
    log = utils.readErrorsFromDisk()
except FileNotFoundError:
    log = {}

# Compare test results to the log
good, bad = utils.getErrorDiff(errors, log)

# Report to the Discord webhooks
if good:
	utils.reportGoodNews(good)

if bad:
	utils.reportBadNews(bad)

# Write the current errors to disk
utils.writeErrorsToDisk(errors)
