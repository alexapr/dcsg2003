# Imports
import utils # Custom library with utility functions
import requests
import subprocess
import time
from http.client import responses

# Functions
def webtest(webaddr):
    """
    Fetches the webpage and reports to the if there are errors
    """
    r = requests.get(f"http://{webaddr}")
    if r.status_code != 200:
        return f"CRITICAL: Webtest - Returned {r.status_code} (expected 200)"
    if "<a href='/showuser.php?user=" not in r.text:
        return f"CRITICAL: Webtest - Users are not displayed"
    return None

def pingtest(ip):
    """
    Pings the servers to verify access
    """
    if subprocess.run(["ping", "-c", "1", ip], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL).returncode != 0:
        return "CRITICAL: Ping"
    else:
        return None


def activetest(host):
    if host.status == "ACTIVE":
        return None
    else:
        c = 0
        while c < 3 and host.status != "ACTIVE":
            utils.startHost(host)
            time.sleep(30)
        if host.status == "ACTIVE":
            return "OK: Host was down, but was successfully started"
        else:
            return "CRITICAL: Host is down, attempts to restart failed"
