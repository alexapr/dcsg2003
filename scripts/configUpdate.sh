#!/usr/bin/bash
for i in {1..2};
do 
  scp ~/dcsg2003/src/config.php www$i:config.php &&
  ssh www$i sudo cp config.php /var/www/html/config.php &&
  ssh www$i sudo service apache2 restart
done;
