#!/bin/bash

# This script should be run on the docker swarm manager

# Build the new image
ssh docker1 "docker build -t bookface:latest /home/ubuntu/dcsg2003"

# Tag and push to the register for access on all nodes
ssh docker1 "docker tag bookface localhost:5000/bookface && docker push localhost:5000/bookface"

# Gather information from openstack
OS_SERVERS=`openstack server list`

BF_DB_HOST=`echo "$OS_SERVERS" | grep db1 | cut -d '|' -f 5 | cut -d '=' -f 2`
BF_WEBHOST=`echo "$OS_SERVERS" | grep balancer | cut -d '|' -f 5 | cut -d '=' -f 2 | cut -d ',' -f 1`
BF_MEMCACHE_SERVER=`echo "$OS_SERVERS" | grep docker1 | cut -d '|' -f 5 | cut -d '=' -f 2`

# Set frontpage limit
BF_FRONTPAGE_LIMIT=5000

echo $BF_DB_HOST
echo $BF_WEBHOST
echo $BF_MEMCACHE_SERVER
echo $BF_FRONTPAGE_LIMIT

# Remove old service and start the new build
ssh docker1 "docker service rm bookface && \
             docker service create --name bookface \
	     -e BF_DB_HOST=\"$BF_DB_HOST\" \
	     -e BF_WEBHOST=\"$BF_WEBHOST\" \
	     -e BF_FRONTPAGE_LIMIT=\"$BF_FRONTPAGE_LIMIT\" \
	     -e BF_MEMCACHE_SERVER=\"$BF_MEMCACHE_SERVER\" \
	     -p published=8080,target=80 \
	     --replicas 8 \
	     localhost:5000/bookface"
