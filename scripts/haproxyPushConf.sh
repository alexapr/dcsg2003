#!/bin/bash

IP=192.168.132.136

scp ~/dcsg2003/cfg/haproxy.cfg $IP:~
ssh $IP "sudo mv ~/haproxy.cfg /etc/haproxy/haproxy.cfg; sudo systemctl restart haproxy"
