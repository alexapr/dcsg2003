Oppsett av Databasen:

- Lagde DB server i openstack
    - t1.medium da vi mistenker denne trenger litt ekstra ressurser
- Kjørte oppdatering på maskinen
db:~$ sudo apt update && sudo apt upgrade

- Installerte cockroachDB

db:~$ curl https://binaries.cockroachdb.com/cockroach-v21.2.4.linux-amd64.tgz | tar -xz && sudo cp -i cockroach-v21.2.4.linux-amd64/cockroach /usr/local/bin/

- Verifiserte installasjonen med
db:~$ cockroach version

- Lagde /bfdata
db:~$ sudo mkdir /bfdata

- Startet databasen
db:~$ sudo cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=localhost:26257


- Initialiserte databasen
db:~$ cockroach init --insecure --host=localhost:26257

- Installerte net-tools (for 
db:~$ sudo apt install net-tools

- Verifiserte at den kjører og lytter


Oppsett av webserverene:

1.
omkonfigurerer webservere

-	kopierer repoet https://git.cs.oslomet.no/kyrre.begnum/bookface.git, kjører 
    komandoene:
  
		git clone  https://git.cs.oslomet.no/kyrre.begnum/bookface.git
		cd bookface

- 	bytter ut gammel index i webserveren med ny kode:
		
		sudo rm /var/www/html/index.html
		sudo cp code/* /var/www/html/

- 	lager en kopi av config fila som nå har blitt lagt til i index området:

		cp config_example.php /var/www/html/config.php
		
-   konfigurerer config.php med informasjon om databasen og balancer'en

		<?php
		$use_local_images = 1;
		$dbhost = getenv("192.168.128.20");
		$dbport =  getenv("205564");
		$db = getenv("bf");
		$dbuser = getenv("bfusersecure");                                                                                       
		$webhost = getenv("10.212.139.233");
		$weburl = 'http://' . $webhost ;
		?>

- 	Begynner å teste om oppsettet fungerer, ved å kopierer HTML fra webserveren

	curl http://floating-i


Videre testing av Oppsettet:

db:~$ ps aux | grep cockroach
root        1835  2.1 27.5 1018292 205564 pts/0  Sl   08:51   0:25 cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --join=localhost:26257
ubuntu      2729  0.0  0.0   8160   736 pts/0    S+   09:11   0:00 grep --color=auto cockroach

db:~$ sudo netstat -antlp
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      18669/systemd-resol 
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      18405/sshd: /usr/sb 
tcp        0      0 127.0.0.1:39136         127.0.0.1:26257         ESTABLISHED 1835/cockroach      
tcp        0    304 192.168.128.20:22       192.168.132.99:48080    ESTABLISHED 1594/sshd: ubuntu [ 
tcp        0      0 192.168.128.20:45920    91.189.88.245:80        TIME_WAIT   -                   
tcp        0      0 127.0.0.1:39138         127.0.0.1:26257         ESTABLISHED 1835/cockroach      
tcp6       0      0 :::8080                 :::*                    LISTEN      1835/cockroach      
tcp6       0      0 :::26257                :::*                    LISTEN      1835/cockroach      
tcp6       0      0 :::22                   :::*                    LISTEN      18405/sshd: /usr/sb 
tcp6       0      0 127.0.0.1:26257         127.0.0.1:39136         ESTABLISHED 1835/cockroach      
tcp6       0      0 127.0.0.1:26257         127.0.0.1:39138         ESTABLISHED 1835/cockroach 


- Initialized the database (again ish)

db:~$ cockroach sql --insecure --host=localhost:26257

root@localhost:26257/defaultdb> CREATE DATABASE bf;
[...]
root@localhost:26257/defaultdb> CREATE USER bfusersecure;
[...]
root@localhost:26257/defaultdb> GRANT ALL ON DATABASE bf TO bfusersecure;
[...]
root@localhost:26257/bf> CREATE table users ( userID INT PRIMARY KEY DEFAULT unique_rowid(), name STRING
(50), picture STRING(300), status STRING(10), posts INT, comments INT, lastPostDate TIMESTAMP DEFAULT NO
W(), createDate TIMESTAMP DEFAULT NOW());
CREATE table posts ( postID INT PRIMARY KEY DEFAULT unique_rowid(), userID INT, text STRING(300), name S
TRING(150), postDate TIMESTAMP DEFAULT NOW());
CREATE table comments ( commentID INT PRIMARY KEY DEFAULT unique_rowid(), postID INT, userID INT, text S
TRING(300),  postDate TIMESTAMP DEFAULT NOW());
CREATE table pictures ( pictureID STRING(300), picture BYTES );
[...]
root@localhost:26257/bf> exit

- Sleit med oppsett av php, viste seg at "noen" glemte å installere php-pgsql :upside-down-smiley:


- Sendte mail til Kyrre om klart

- Testet reboot av DB
    - Service starter ikke automatisk ved boot
    - Lager et startup script i /root med:
    
    cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=localhost:26257

    - Åpner crontab

db:~$ sudo crontab -e

- La til følgende i crontabfilen:
    @reboot /root/startup.sh

- Fikk det ikke til å funke så endte opp med å lage en systemd service:


[Unit]
Description=Cockroach Database cluster node
Requires=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --join=localhost:26257
Restart=always
RestartSec=10
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=cockroach
User=root

[Install]
WantedBy=multi-user.target
