Satte opp tre VM'er i OpenStack.
t1.small, Ubuntu 20.04
balancer, www1, www2

Satte opp en floating IP og assignet denne til balancer: 10.212.139.233

La inn de nye VM'ene i .ssh/config filen

Logget inn på www1 og oppdaterte maskinen i en tmux-session med
sudo apt update && sudo apt upgrade

Installerte så apache med følgende kommando:
sudo apt install apache2 libapache2-mod-php

La inn en custom prompt i .bashrc for å lettere identifisere hvilken maskin man jobber på.

Gjore endring i /var/www/html/index.html slik at vi kan bekrefte respons fra www1

Brukte curl fra manager for å bekrefte at apache fungerer med følgende kommando:
curl -v http://192.168.130.133

Fikk forventet respons fra www1, så apache fungerer som ønsket


Logget inn på balancer og installerte haproxy

Endret på /etc/haproxy/haproxy.cfg
  - Satte opp frontend "main", bind til port 80 med default_backend webservers.
  - Sattae opp backend "webservers" med roundrobin balance og serverene www1 og www2
  - Satte opp stats side på port 1337

Sjekket at konfigurasjonen var gyldig med haproxy -c -f /etc/haproxy/haproxy.cfg, vi var litt kjappe på labben og hadde skrevet inn konfigurasjonen vår over defaults.
Flyttet konfigurasjonen til bunn av filen, og fikk godkjent konfigurasjonsfilen.

Startet haproxy med:
sudo systemctl start haproxy

Verifiserte at haproxy lyttet til de korrekte portene (80 og 1936) med:
sudo netstat -anltp

Åpnet port 80 i security groups i OpenStack

Testet at lasbalansereren faktisk serverer www1 og www2 med curl fra manager:
curl -v 192.168.132.136

Gjentok dette et par ganger for å se at vi får respons fra både www1 og www2

Fulgte samme prosedyre for den flytende IP-adressen:
curl -v 10.212.139.233


