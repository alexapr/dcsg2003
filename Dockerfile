#Version 0.1
FROM ubuntu/apache2:2.4-20.04_beta
MAINTAINER alexapr@logntnu.no
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update &&  apt-get install -y apache2 libapache2-mod-php php-mysql git php-memcache curl php-cli php-mbstring unzip php-pgsql wget2

RUN rm /var/www/html/index.html
RUN mkdir /var/www/html/images
ADD src/* /var/www/html/
ADD init.sh /

EXPOSE 80
